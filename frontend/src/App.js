import "./App.css";
import { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    (async () => {
      const posts = await axios.get(
        `${process.env.REACT_APP_API_BASE_URL}/posts`
      );
      setPosts(posts.data);
    })();
  }, []);
  return (
    <div className="App">
      {posts ? (
        posts.map((post) => (
          <div style={{ width: "100%", border: "1px solid gray" }}>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </div>
        ))
      ) : (
        <p>loading...</p>
      )}
    </div>
  );
}

export default App;
