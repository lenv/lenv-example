const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const postController = require("./controllers/postController");

const app = express();

app.use(cors());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/posts", postController.getPosts);

mongoose
  .connect(process.env.MONGO_DB_CONNECTION_URL)
  .then(() => {
    console.log("DB connected!");
  })
  .catch(() => {
    console.log("Error connecting to database");
  });

app.listen(process.env.PORT || 3001, () => {
  console.log(`Server is running on port ${process.env.PORT || 3001} `);
});
