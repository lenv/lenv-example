const Posts = require("../models/post");

const getPosts = async (req, res) => {
  try {
    const posts = await Posts.find({});
    return res.json(posts);
  } catch (error) {
    throw new Error("couldnt find any posts");
  }
};

module.exports = {
  getPosts,
};
