const { Seeder } = require("mongo-seeding");
const path = require("path");

const config = {
  database: process.env.MONGO_DB_CONNECTION_URL,
  dropCollections: true // drop collections before seeding
};

const seeder = new Seeder(config);

const collections = seeder.readCollectionsFromPath(
  path.resolve("./seed/data"),
  {
    transformers: [Seeder.Transformers.replaceDocumentIdWithUnderscoreId],
  }
);

seeder
  .import(collections)
  .then(() => {
    console.log("Success");
  })
  .catch((err) => {
    console.log("Error", err);
    process.exit(1);
  });
